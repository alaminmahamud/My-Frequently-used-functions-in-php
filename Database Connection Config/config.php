<?php
/**
 * Created by PhpStorm.
 * User: Alamin Mahamud
 * Date: 5/23/2015
 * Time: 10:38 PM
 */

/**
 * Database connection
 *
 */

define("DB_HOST", <hostname>);
define("DB_USER", <db_user>);
define("DB_PASS", <db_pass>);
define("DB_DATABASE", <db_name>);

?>